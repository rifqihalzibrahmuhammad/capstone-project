import Master from "./MasterLayout"

import { TooltipProvider } from "@/Components/ui/tooltip"

import Headbar from "@/Components/Headbar"
import Sidebar from "@/Components/Sidebar"
import { PropsWithChildren } from "react"

export default function Authenticated({ children }: PropsWithChildren) {
    return (
        <>
            <Master>
                <TooltipProvider>
                    <div className="grid h-screen w-full pl-[56px]">
                        <Sidebar />
                        <div className="flex flex-col">
                            <Headbar />
                            <main>
                                {children}
                            </main>
                        </div>
                    </div>
                </TooltipProvider>
            </Master>
        </>
    )
}