import { PropsWithChildren } from 'react';
import { ThemeProvider } from "@/Components/ThemeProvider";

export default function Master({ children }: PropsWithChildren) {
    return (
        <ThemeProvider defaultTheme="dark" storageKey="vite-ui-theme">
            {children}
        </ThemeProvider>
    );
}
