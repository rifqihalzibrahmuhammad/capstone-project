import Authenticated from "@/Layouts/AuthenticatedLayout"
import { Head } from "@inertiajs/react"
import { DollarSign } from "lucide-react"
import { Card, CardContent, CardHeader, CardTitle, } from "@/Components/ui/card"
import { usePage } from "@inertiajs/react"

export default function Index(props) {
    const { reports } = props
    const reportCount = reports.length // Get the count of all reports

    // Filter out reports with 'approved' status and get the count
    const approvedReportCount = reports.filter(report => report.status === 'approved').length
    const pendingReportCount = reports.filter(report => report.status === 'pending').length
    const rejectedReportCount = reports.filter(report => report.status === 'rejected').length

    // Get the auth data from the usePage hook
    const { auth } = usePage().props;
    const name = auth.user.name
    const email = auth.user.email
    const role = auth.user.role

    return (
        <>
            <Head title="Dashboard" />
            <Authenticated>
                <div className="grid grid-cols-1 md:grid-cols-4 p-4 gap-4">
                    <Card className="text-center">
                        <CardHeader>
                            <CardTitle>Profile Informations</CardTitle>
                        </CardHeader>
                        <CardContent>
                            <div className="flex flex-col items-center">
                                {/* Avatar image */}
                                <img
                                    src="https://media.istockphoto.com/vectors/profile-placeholder-image-gray-silhouette-no-photo-vector-id1016744004?k=6&m=1016744004&s=612x612&w=0&h=L-6pLRmsCm-LHeaC493SSAOSB5ftQB8s3MX87WrktMU="
                                    alt="Avatar"
                                    className="w-24 h-24 rounded-full mb-2 mt-2"
                                />
                                <div className="text-xl font-bold">{name}</div>
                                <div>{email}</div>
                                <p className="text-muted-foreground">{role}</p>
                                <div className="mt-6">Total Identifications:</div>
                                <div className="text-xl font-bold">{reportCount}</div>

                            </div>
                        </CardContent>
                    </Card>
                    <div>
                        <Card>
                            <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
                                <CardTitle className="text-sm font-medium">Approved Identifications</CardTitle>
                            </CardHeader>
                            <CardContent>
                                <div className="text-2xl font-bold">{approvedReportCount}</div>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card>
                            <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
                                <CardTitle className="text-sm font-medium">Pending Identifications</CardTitle>
                            </CardHeader>
                            <CardContent>
                                <div className="text-2xl font-bold">{pendingReportCount}</div>
                            </CardContent>
                        </Card>
                    </div>
                    <div>
                        <Card>
                            <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
                                <CardTitle className="text-sm font-medium">Rejected Identifications</CardTitle>
                            </CardHeader>
                            <CardContent>
                                <div className="text-2xl font-bold">{rejectedReportCount}</div>
                            </CardContent>
                        </Card>
                    </div>
                </div>
            </Authenticated>
        </>
    )
}