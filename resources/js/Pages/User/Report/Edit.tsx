import { Button } from "@/Components/ui/button"
import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from "@/Components/ui/card"
import { Label } from "@/Components/ui/label"
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/Components/ui/select"
import Authenticated from "@/Layouts/AuthenticatedLayout"
import { Head, Link } from "@inertiajs/react"

export default function Detail(props) {
    const { report } = props

    const imageUrl = report.image_url
    const reportTitle = report.title
    const reportDescription = report.description
    const sketcher = report.sketcher.name
    const prompt = report.prompt
    const parts = prompt.split(',');

    // Split each string in the array by commas and flatten the array
    let flattenedArr = parts.map(item => item.split(',')).flat();

    // Join elements from index 16 onwards
    let joinedString = flattenedArr.slice(16).join(', ');

    // Create the final array with the first 16 elements plus the joined string
    let finalArr = flattenedArr.slice(0, 16).concat(joinedString);

    const headers = [
        "Gender",
        "Age",
        "Skin type",
        "Face shape",
        "Hair",
        "Forehead shape",
        "Eyebrow",
        "Eyes shape",
        "Eyes color",
        "Nose",
        "Lip",
        "Chin",
        "Ears",
        "Additional"
    ];

    return (
        <>
            <Head title="Report Edit" />

            <Authenticated>
                <div className="p-4">
                    <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-12">
                        <div className="flex items-center mb-8 justify-between">
                            <h1 className="text-2xl font-semibold md:text-3xl">Report Detail</h1>
                            <div className="flex gap-1 justify-end">
                                <Link href={route('report.index')}>
                                    <Button type="button">
                                        Back
                                    </Button>
                                </Link>
                            </div>
                        </div>
                        <div className="grid grid-cols-1 lg:grid-cols-4 gap-8">
                            <div className="lg:col-span-1">
                                <img
                                    src={imageUrl}
                                    alt="Generated Image"
                                    className="w-full aspect-square rounded-lg shadow-md"
                                />
                            </div>
                            <div className="lg:col-span-3">
                                <Card className="flex flex-col gap-4 p-6 rounded-lg shadow-md">
                                    <CardHeader className="grid grid-cols-1 lg:grid-cols-4">
                                        <div className="col-span-3">
                                            <CardTitle>{reportTitle}</CardTitle>
                                            <CardDescription>{reportDescription}</CardDescription>
                                        </div>
                                    </CardHeader>
                                    <CardContent>
                                        <table className="w-full">
                                            <tbody>
                                                {headers.map((header, index) => {
                                                    if (index < finalArr.length - 3) {
                                                        return (
                                                            <tr key={index} className="border-b border-gray-200 last:border-none">
                                                                <td className="py-2 pr-4 font-semibold text-left">{header}</td>
                                                                <td className="py-2">:</td>
                                                                <td className="py-2 text-left">{finalArr[index + 3]}</td>
                                                            </tr>
                                                        );
                                                    }
                                                    return null;
                                                }).filter(row => row !== null)}
                                            </tbody>
                                        </table>
                                    </CardContent>
                                    <CardFooter className="flex justify-end">
                                        {/* Footer content if any */}
                                        <div className="text-center">
                                            <div className="mb-5 font-semibold">Sketcher</div>
                                            <div>{sketcher}</div>
                                        </div>
                                    </CardFooter>
                                </Card>
                            </div>
                        </div>
                        <div className="mt-8">
                            <div className="text-center text-xl font-semibold mb-4">Similarity</div>
                            <div className="grid grid-cols-1 lg:grid-cols-3 gap-6">
                                {report.ai_analysis.map((item, index) => (
                                    <div key={index} className="rounded-lg shadow-md overflow-hidden">
                                        <img
                                            alt="Similarity Image"
                                            className="w-full aspect-square object-cover"
                                            src={item.people.image_url}
                                        />
                                        <div className="p-4 text-center">
                                            <p className="font-semibold">{item.people.unique_id}</p>
                                            <p className="font-semibold">{item.people.name}</p>
                                            <p>Age: {item.people.age}</p>
                                            <p>Gender: {item.people.gender}</p>
                                            <p>Address: {item.people.address}</p>
                                            <p>Similarity: {parseFloat(item.confidence_percentage).toFixed(2)}</p>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </Authenticated>
        </>
    )
}