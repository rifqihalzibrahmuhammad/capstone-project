import { DataTable } from "@/Components/ui/data-table"
import Authenticated from "@/Layouts/AuthenticatedLayout"
import { Head } from "@inertiajs/react"
import { Columns } from "./Columns"

export default function Index(props) {
    const { reports } = props

    return (
        <>
            <Head title="Report" />

            <Authenticated>
                <div className="p-4">
                    <DataTable columns={Columns} data={reports} />
                </div>
            </Authenticated>
        </>
    )
}