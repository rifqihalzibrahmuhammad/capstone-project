import { ColumnDef } from "@tanstack/react-table";
import { ArrowUpDown, Edit, Trash } from "lucide-react";

import DangerButton from "@/Components/DangerButton";
import InputError from "@/Components/InputError";
import Modal from "@/Components/Modal";
import SecondaryButton from "@/Components/SecondaryButton";
import { Button } from "@/Components/ui/button";
import { Input } from "@/Components/ui/input";
import { Label } from "@/Components/ui/label";
import { Link, useForm } from "@inertiajs/react";
import { FormEventHandler, useState } from "react";
import { Dialog, DialogContent, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/Components/ui/dialog";
import InputLabel from "@/Components/InputLabel";
import { Tooltip, TooltipContent, TooltipTrigger } from "@/Components/ui/tooltip";

export type Users = {
    email: string;
    name: string;
    role: string;
};

export const Columns: ColumnDef<Users>[] = [
    {
        accessorKey: "name",
        header: ({ column }) => (
            <Button
                variant="ghost"
                onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
            >
                Name
                <ArrowUpDown className="ml-2 h-4 w-4" />
            </Button>
        ),
    },
    {
        accessorKey: "email",
        header: ({ column }) => (
            <Button
                variant="ghost"
                onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
            >
                Email
                <ArrowUpDown className="ml-2 h-4 w-4" />
            </Button>
        ),
    },
    {
        accessorKey: "role",
        header: "Role",
    },
    {
        id: "actions",
        header: "Actions",
        cell: ({ row }) => {
            const user = row.original;
            const [confirmingUserDeletion, setConfirmingUserDeletion] = useState(false);

            const {
                data,
                setData,
                delete: destroy,
                processing,
                reset,
                errors,
            } = useForm({
                id: user.id,
            });

            const confirmUserDeletion = () => {
                setConfirmingUserDeletion(true);
            };

            const closeModal = () => {
                setConfirmingUserDeletion(false);

                reset();
            };

            const onDelete: FormEventHandler = (e) => {
                e.preventDefault();

                destroy(route('admin.user.destroy'), {
                    preserveScroll: true,
                    onSuccess: () => closeModal(),
                    onFinish: () => reset(),
                });
            };

            return (
                <>
                    <Tooltip>
                        <TooltipTrigger asChild>
                            <Link href={route('admin.user.edit', user.id)}>
                                <Button
                                    variant="ghost"
                                    size="icon"
                                >
                                    <Edit className="size-5" />
                                </Button>
                            </Link>
                        </TooltipTrigger>
                        <TooltipContent side="right" sideOffset={5}>
                            Edit
                        </TooltipContent>
                    </Tooltip>

                    <Dialog>
                        <DialogTrigger asChild>
                            <Button
                                variant="ghost"
                                size="icon"
                            >
                                <Trash className="size-5" />
                            </Button>
                        </DialogTrigger>
                        <DialogContent>
                            <DialogHeader>
                                <DialogTitle>
                                    Are you sure you want to delete this account?
                                </DialogTitle>
                            </DialogHeader>
                            <form onSubmit={onDelete}>
                                Once this account is deleted, all of its resources and data will be permanently deleted. Please
                                enter your password to confirm you would like to permanently delete this account.

                                <div className="mt-6 hidden">
                                    <Label htmlFor="name">Id</Label>
                                    <Input
                                        id="id"
                                        type="text"
                                        name="id"
                                        value={data.id}
                                        className="mt-1 block w-full"
                                        autoComplete="id"
                                        onChange={(e) => setData('id', e.target.value)}
                                    />
                                    <InputError message={errors.id} />
                                </div>

                                <DialogFooter className="mt-6">
                                    <DangerButton disabled={processing}>
                                        Delete Account
                                    </DangerButton>
                                </DialogFooter>
                            </form>
                        </DialogContent>
                    </Dialog>
                </>
            );
        },
    },
];
