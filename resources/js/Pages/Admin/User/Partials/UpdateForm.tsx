import { Link, useForm } from '@inertiajs/react';

import InputError from "@/Components/InputError";
import { Button } from "@/Components/ui/button";
import { Input } from "@/Components/ui/input";
import { Label } from "@/Components/ui/label";
import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/Components/ui/select";

interface UpdateFormProps {
    initialData: User | null;
}

export const UpdateForm: React.FC<UpdateFormProps> = ({ initialData }) => {

    const { data, setData, patch, errors } = useForm({
        name: (initialData && initialData.name) || "",
        email: (initialData && initialData.email) || "",
        role: (initialData && initialData.role) || "",
        password: "",
    });

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        patch(route('admin.user.update', initialData.id), {
            onSuccess: () => {
                console.log("Form submitted successfully");
                // Handle success actions here
            },
            onError: (errors) => {
                console.error("Form submission error:", errors);
                // Handle error actions here
            },
        });
    };

    const handleRoleChange = (value: string) => {
        setData('role', value);
    };

    return (
        <form onSubmit={onSubmit} className="space-y-4" method='PUT'>
            <div>
                <Label htmlFor="name">Name</Label>
                <Input
                    id="name"
                    type="text"
                    name="name"
                    value={data.name}
                    className="mt-1 block w-full"
                    autoComplete="name"
                    onChange={(e) => setData('name', e.target.value)}
                />
                <InputError message={errors.name} />
            </div>

            <div>
                <Label htmlFor="password">Password</Label>
                <Input
                    id="password"
                    type="password"
                    name="password"
                    value={data.password}
                    className="mt-1 block w-full"
                    autoComplete="password"
                    onChange={(e) => setData('password', e.target.value)}
                />
                <InputError message={errors.password} />
            </div>

            <div>
                <Label htmlFor="email">Email</Label>
                <Input
                    id="email"
                    type="email"
                    name="email"
                    value={data.email}
                    className="mt-1 block w-full"
                    autoComplete="email"
                    onChange={(e) => setData('email', e.target.value)}
                />
                <InputError message={errors.email} />
            </div>

            <div>
                <Label htmlFor="role">Role</Label>
                <Select value={data.role} onValueChange={handleRoleChange}>
                    <SelectTrigger>
                        <SelectValue placeholder="Select a role" />
                    </SelectTrigger>
                    <SelectContent>
                        <SelectItem value="admin">Admin</SelectItem>
                        <SelectItem value="user">User</SelectItem>
                    </SelectContent>
                </Select>
            </div>

            <div className="flex gap-1 justify-end">
                <Link href={route('admin.user.index')}>
                    <Button type="button" variant="outline">
                        Back
                    </Button>
                </Link>
                <Button type="submit">Save</Button>
            </div>
        </form>
    )
}
