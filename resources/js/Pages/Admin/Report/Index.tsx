import Admin from "@/Layouts/AdminLayout"
import { Head } from "@inertiajs/react"
import { DataTable } from "@/Components/ui/data-table"
import { Columns } from "./Columns"

export default function Index(props) {
    const { reports } = props

    return (
        <>
            <Head title="Admin | Report" />
            <Admin>
                <div className="flex items-center">
                    <h1 className="text-lg font-semibold md:text-2xl">Report</h1>
                </div>
                <div className="flex flex-1 justify-center rounded-lg border border-dashed shadow-sm">
                    <div className="flex flex-col gap-1 w-full p-4">
                        <DataTable columns={Columns} data={reports} />
                    </div>
                </div>
            </Admin>
        </>
    )
}