import { Link, router, useForm } from '@inertiajs/react';

import InputError from "@/Components/InputError";
import { Button } from "@/Components/ui/button";
import { Input } from "@/Components/ui/input";
import { Label } from "@/Components/ui/label";
import { useState } from 'react';
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/Components/ui/select';

interface UpdateFormProps {
    initialData: People | null;
}

export const UpdateForm: React.FC<UpdateFormProps> = ({ initialData }) => {
    const { data, setData, post, errors } = useForm({
        id: (initialData && initialData.id) || "",
        unique_id: (initialData && initialData.unique_id) || "",
        name: (initialData && initialData.name) || "",
        gender: (initialData && initialData.gender) || "",
        age: (initialData && initialData.age) || "",
        address: (initialData && initialData.address) || "",
        image: null, // Add file property to state
    });

    // State to hold preview image URL
    const [previewImage, setPreviewImage] = useState<string | null>(null);

    const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files[0]) {
            const imageFile = e.target.files[0];
            setData('image', imageFile); // Set image file in the form data
            setPreviewImage(URL.createObjectURL(imageFile)); // Set preview image URL
        }
    };

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        post(route('admin.people.update', data.id), {
            onSuccess: () => {
                // Optionally, you can add additional logic here if needed
            },
            onError: (errors) => {
                // Handle errors if needed
            },
            data: data,
            preserveScroll: true,
        });
    };

    const handleGenderChange = (value: string) => {
        setData('gender', value);
    };

    return (
        <form onSubmit={onSubmit} className="space-y-4">

            <div>
                <Label htmlFor="name">Unique ID</Label>
                <Input
                    id="unique_id"
                    type="text"
                    name="unique_id"
                    value={data.unique_id}
                    className="mt-1 block w-full"
                    disabled
                    autoComplete="unique_id"
                    onChange={(e) => setData('unique_id', e.target.value)}
                />
                <InputError message={errors.unique_id} />
            </div>
            <div>
                <Label htmlFor="name">Name</Label>
                <Input
                    id="name"
                    type="text"
                    name="name"
                    value={data.name}
                    className="mt-1 block w-full"
                    autoComplete="name"
                    onChange={(e) => setData('name', e.target.value)}
                />
                <InputError message={errors.name} />
            </div>

            <div>
                <Label htmlFor="gender">Gender</Label>
                <Select value={data.gender} onValueChange={handleGenderChange}>
                    <SelectTrigger>
                        <SelectValue placeholder="Select gender" />
                    </SelectTrigger>
                    <SelectContent>
                        <SelectItem value="male">Male</SelectItem>
                        <SelectItem value="female">Female</SelectItem>
                    </SelectContent>
                </Select>
                <InputError message={errors.gender} />
            </div>

            <div>
                <Label htmlFor="age">Age</Label>
                <Input
                    id="age"
                    type="number"
                    name="age"
                    value={data.age}
                    className="mt-1 block w-full"
                    autoComplete="age"
                    onChange={(e) => setData('age', e.target.value)}
                />
                <InputError message={errors.age} />
            </div>

            <div>
                <Label htmlFor="name">Address</Label>
                <Input
                    id="address"
                    type="text"
                    name="address"
                    value={data.address}
                    className="mt-1 block w-full"
                    autoComplete="address"
                    onChange={(e) => setData('address', e.target.value)}
                />
                <InputError message={errors.address} />
            </div>

            <div>
                <Label htmlFor="image">Face image</Label>
                <Input
                    type="file"
                    id="image"
                    accept="image/*"
                    onChange={handleImageChange} // Call handleImageChange on file input change
                />
                {(previewImage || initialData.image_url) && ( // Check if previewImage or initialData.image_url exists
                    <div className="mt-4">
                        <Label>Image Preview</Label>
                        <img
                            src={previewImage ? previewImage : initialData.image_url} // Use previewImage if it exists, otherwise use initialData.image_url
                            alt="Preview"
                            className="mt-2 h-96"
                        />
                    </div>
                )}
            </div>

            <div className="flex gap-1 justify-end">
                <Link href={route('admin.people.index')}>
                    <Button type="button" variant="outline">
                        Back
                    </Button>
                </Link>
                <Button type="submit">Save</Button>
            </div>
        </form>
    )
}
