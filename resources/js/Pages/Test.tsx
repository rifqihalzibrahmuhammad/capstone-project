import { Head } from "@inertiajs/react"
import Admin from "@/Layouts/AdminLayout"
import User from "@/Layouts/UserLayout"

export default function Test() {
    return (
        <>
            <Head title="Test" />
            {/* <User /> */}
            <Admin />
        </>
    )
}