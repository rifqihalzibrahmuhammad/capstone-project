import { Bar } from "react-chartjs-2";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from "chart.js";

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
)

export default function Charts({ data }) {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const monthlyData = new Array(12).fill(0); // Initialize an array with 12 zeros

    // Extract the total value for the month
    data.forEach(item => {
        const month = new Date(item.month).getMonth();
        monthlyData[month] = item.total;
    });

    const chartData = {
        labels: monthNames,
        datasets: [
            {
                label: "Identifications",
                data: monthlyData,
                backgroundColor: "rgba(0, 0, 0, 0.2)", // Background color set to black
                borderColor: "rgba(0, 0, 0, 1)", // Border color set to black
                borderWidth: 1,
            }
        ]
    }

    return (
        <>
            <Bar options={{}} data={chartData} />
        </>
    )
}