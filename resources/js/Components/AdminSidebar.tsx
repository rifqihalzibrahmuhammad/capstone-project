import {
    CircuitBoard,
    Fingerprint,
    Home,
    LineChart,
    ScanFace,
    User
} from "lucide-react"

import { cn } from "@/lib/utils"

import { Link, usePage } from "@inertiajs/react"

export default function AdminSidebar() {
    const { url } = usePage(); // Get the current route URL
    const pathname = url.split('?')[0]; // Remove any query parameters, keeping only the path

    return (
        <div className="hidden border-r bg-muted/40 md:block">
            <div className="flex h-full max-h-screen flex-col gap-2">
                <div className="flex h-14 items-center border-b px-4 lg:h-[60px] lg:px-6">
                    <Link href={route('admin.dashboard.index')} className="flex items-center gap-2 font-semibold">
                        <Fingerprint className="h-6 w-6" />
                        <span className="">SuspectX</span>
                    </Link>
                </div>
                <div className="flex-1">
                    <nav className="grid items-start px-2 text-sm font-medium lg:px-4">
                        <Link
                            href={route('admin.dashboard.index')}
                            className={cn(
                                'flex items-center gap-3 rounded-lg px-3 py-2 text-muted-foreground transition-all',
                                pathname === '/admin' && 'bg-muted text-primary',
                                pathname !== '/admin' && 'hover:text-primary'
                            )}
                        >
                            <Home className="h-4 w-4" />
                            Dashboard
                        </Link>
                        <Link
                            href={route('admin.user.index')}
                            className={cn(
                                'flex items-center gap-3 rounded-lg px-3 py-2 text-muted-foreground transition-all',
                                pathname === '/admin/user' && 'bg-muted text-primary',
                                pathname !== '/admin/user' && 'hover:text-primary'
                            )}
                        >
                            <User className="h-4 w-4" />
                            User
                        </Link>
                        <Link
                            href={route('admin.people.index')}
                            className={cn(
                                'flex items-center gap-3 rounded-lg px-3 py-2 text-muted-foreground transition-all',
                                pathname === '/admin/people' && 'bg-muted text-primary',
                                pathname !== '/admin/people' && 'hover:text-primary'
                            )}
                        >
                            <ScanFace className="h-4 w-4" />
                            People
                        </Link>
                        <Link
                            href={route('admin.report.index')}
                            className={cn(
                                'flex items-center gap-3 rounded-lg px-3 py-2 text-muted-foreground transition-all',
                                pathname === '/admin/report' && 'bg-muted text-primary',
                                pathname !== '/admin/report' && 'hover:text-primary'
                            )}
                        >
                            <LineChart className="h-4 w-4" />
                            Report
                        </Link>
                        <Link
                            href={route('dashboard.index')}
                            className="flex items-center gap-3 rounded-lg px-3 py-2 text-muted-foreground transition-all hover:text-primary"
                        >
                            <CircuitBoard className="h-4 w-4" />
                            Get Started
                        </Link>
                    </nav>
                </div>
            </div>
        </div>
    )
}
