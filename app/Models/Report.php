<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Report extends Model
{
    use HasFactory;

    protected $table = 'reports';
    protected $fillable = [
        'id',
        'title',
        'description',
        'prompt',
        'remark',
        'image_url',
        'proof_image_url',
        'status',
        'created_by',
        'approved_by'
    ];

    public function approver(): BelongsTo
    {
        return $this->belongsTo(User::class, 'approved_by', 'id');
    }

    public function sketcher(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function aiAnalysis(): HasMany
    {
        return $this->hasMany(AiAnalysis::class);
    }
}
