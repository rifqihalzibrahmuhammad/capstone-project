<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class AdminUserController extends Controller
{
    public function index(): Response
    {
        $users = User::all()->map(function ($user) {
            return [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'role' => $user->role,
            ];
        });

        $create_url = route('admin.user.create');

        return Inertia::render('Admin/User/Index', [
            'users' => $users,
            'create_url' => $create_url,
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/User/Form');
    }

    public function edit(string $id): Response
    {
        $user = User::findOrFail($id);
        return Inertia::render('Admin/User/Form', [
            'user' => $user
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'role' => 'required|string|max:255',
            'password' => 'nullable|string|min:8',
        ]);

        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'role' => $validatedData['role'],
            'password' => bcrypt($validatedData['password']),
        ]);

        // You can optionally return a response or redirect as needed
        return redirect()->intended(route('admin.user.index', absolute: false));
    }

    public function update(Request $request, string $id): RedirectResponse
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'role' => 'required|string|max:255',
            'password' => 'nullable|string|min:8',
        ]);

        $user = User::findOrFail($id);

        $user->name = $validatedData['name'];
        $user->email = $validatedData['email'];
        $user->role = $validatedData['role'];
        $user->password = bcrypt($validatedData['password']);

        $user->save();

        return redirect()->intended(route('admin.user.index', [], false));
    }

    public function destroy(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'id' => 'required',
        ]);

        $user = User::findOrFail($validatedData['id']);

        $user->delete();

        return redirect()->intended(route('admin.user.index', [], false));
    }
}
