<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Inertia\Response;

class AdminReportController extends Controller
{
    public function index(): Response
    {
        $reports = Report::all()->map(function ($report) {
            return [
                'id' => $report->id,
                'title' => $report->title,
                'description' => $report->description,
                'status' => $report->status,
            ];
        });

        return Inertia::render('Admin/Report/Index', [
            'reports' => $reports,
        ]);
    }

    public function approve(string $id): Response
    {
        $report = Report::with(['approver', 'sketcher', 'aiAnalysis.people'])->findOrFail($id);
        return Inertia::render('Admin/Report/Approval', [
            'report' => $report
        ]);
    }

    public function update(Request $request, string $id): RedirectResponse
    {
        try {
            $validatedData = $request->validate([
                'status' => 'required|string',
                'remark' => 'required|string',
                'image' => 'required|image',
            ]);

            $imageUrl = '';

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = time() . $file->getClientOriginalExtension();
                // Handle file upload to AWS S3
                Storage::disk('s3')->put('proof/' . $filename, file_get_contents($file), 'public');
                $imageUrl = Storage::disk('s3')->url('proof/' . $filename);
            } else {
                throw new \Exception("Image is required.");
            }

            $report = Report::findOrFail($id);

            $report->status = $validatedData['status'];
            $report->remark = $validatedData['remark'];
            $report->approved_by = Auth::id();
            $report->proof_image_url = $imageUrl;

            $report->save();

            return redirect()->intended(route('admin.report.index', [], false));
        } catch (\Exception $e) {
            // Handle exceptions and validation errors
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
    }
}
