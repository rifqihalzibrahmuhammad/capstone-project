<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class IdentifyController extends Controller
{
    public function index(Request $request)
    {
        // Check if the request method is GET
        if ($request->isMethod('get')) {
            return to_route('generate.index');
        }

        // Access the data from the request
        $identify_response = $request->all(); // This will give you all the data passed with the request

        // Now you can use $identify_response as needed

        return Inertia::render('User/Identify/Index', [
            'identify_response' => $identify_response,
        ]);
    }
}
