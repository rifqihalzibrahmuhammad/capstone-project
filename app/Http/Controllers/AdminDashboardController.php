<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class AdminDashboardController extends Controller
{
    public function index(): Response
    {
        $reportsPerMonth = Report::selectRaw('DATE_TRUNC(\'month\', created_at) AS month, COUNT(*) AS total')
            ->groupBy('month')
            ->get()
            ->toArray();

        $reports = Report::all()->map(function ($report) {
            return [
                'id' => $report->id,
                'title' => $report->title,
                'description' => $report->description,
                'status' => $report->status,
            ];
        });

        return Inertia::render('Admin/Dashboard/Index', [
            'reports' => $reports,
            'reportsPerMonth' => $reportsPerMonth,
        ]);
    }
}
