<?php

namespace App\Http\Controllers;

use App\Models\People;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class AdminPeopleController extends Controller
{
    public function index(): Response
    {
        $peoples = People::all()->map(function ($people) {
            return [
                'id' => $people->id,
                'unique_id' => $people->unique_id,
                'name' => $people->name,
                'gender' => $people->gender,
                'age' => $people->age,
                'address' => $people->address,
            ];
        });

        $create_url = route('admin.people.create');

        return Inertia::render('Admin/People/Index', [
            'peoples' => $peoples,
            'create_url' => $create_url,
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/People/Form');
    }

    public function edit(string $id): Response
    {
        $people = People::findOrFail($id);
        return Inertia::render('Admin/People/Form', [
            'people' => $people
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'unique_id' => 'required|string|size:12|unique:peoples',
            'name' => 'required|string',
            'gender' => 'required|string',
            'age' => 'required|numeric',
            'address' => 'required|string',
            'image' => 'required|image',
        ]);

        $image = $request->file('image');

        $response = Http::asMultipart()->attach(
            'image',
            file_get_contents($image->getRealPath()),
            $image->getClientOriginalName()
        )->post('http://127.0.0.1:8000/api/create-people', [
            'unique_id' => $validatedData['unique_id'],
            'name' => $validatedData['name'],
            'gender' => $validatedData['gender'],
            'age' => $validatedData['age'],
            'address' => $validatedData['address'],
        ]);

        if ($response->successful()) {
            // Handle successful response
            return redirect()->route('admin.people.index')->with('success', 'People created successfully');
        } else {
            // Handle error response
            return redirect()->back()->withErrors(['error' => 'Failed to create people']);
        }
    }

    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'name' => 'nullable|string',
            'gender' => 'nullable|string',
            'age' => 'nullable|integer',
            'address' => 'required|string',
            'image' => 'nullable|image',
        ]);

        $image = $request->file('image');

        if ($image) {
            // If image is not null, send a multipart request with the image file attached
            $response = Http::asMultipart()->attach(
                'image',
                file_get_contents($image->getRealPath()),
                $image->getClientOriginalName()
            )->put('http://127.0.0.1:8000/api/update-people/' . $id, [
                'name' => $validatedData['name'],
                'gender' => $validatedData['gender'],
                'age' => $validatedData['age'],
                'address' => $validatedData['address'],
            ]);
        } else {
            // If image is null, send a request without the image
            $response = Http::asForm()->put('http://127.0.0.1:8000/api/update-people/' . $id, [
                'image' => null,
                'name' => $validatedData['name'],
                'gender' => $validatedData['gender'],
                'age' => $validatedData['age'],
                'address' => $validatedData['address'],
            ]);
        }

        if ($response->successful()) {
            // Handle successful response
            return redirect()->route('admin.people.index')->with('success', 'People updated successfully');
        } else {
            // Handle error response
            return redirect()->back()->withErrors(['error' => 'Failed to update people']);
        }
    }

    public function destroy(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'id' => 'required',
        ]);

        $people = People::findOrFail($validatedData['id']);

        $people->delete();

        return redirect()->intended(route('admin.people.index', [], false));
    }
}
